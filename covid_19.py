# covid_19.py
# Omkar H. Ramachandran
# ramach21@egr.msu.edu
#
#

import numpy as np
import csv
import sys
from datetime import date

def linear_reg(x,y):
    A = np.vstack([x, np.ones(len(x))]).T
    m, c = np.linalg.lstsq(A, np.log2(y), rcond=None)[0]
    return m,c

def covid19_model(fname,country_name,regression_days,start_regression_day):
    csv_reader = []
    data = []
    min_date = []

    with open(fname, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for rows in csv_reader:
            date_string = rows["Date"]
            month_string = date_string[:2]
            day_string = date_string[3:5]
            year_string = date_string[6:]
            date_obj = date.fromisoformat(year_string+"-"+month_string+"-"+day_string) 
            if(line_count == 0):
                min_date = date_obj
                line_count += 1
            if(rows["Country"] == country_name):
                data.append([date_obj,int(float(rows["Deaths"])),int(float(rows["Recovered"])),int(float(rows["Confirmed"]))])


    data = np.array(data)
    date_array = data[:,0]
    total_days = int((max(date_array)-min_date).total_seconds()/(3600.0*24))
    deaths_array = np.zeros([total_days+1])
    recovered_array = np.zeros([total_days+1])
    cases_array = np.zeros([total_days+1])
    min_date_country = min(date_array)
    for i in range(len(data)):
        days_passed = int((date_array[i]-min_date).total_seconds()/(3600.0*24))
        deaths_array[days_passed] += data[i,1]
        recovered_array[days_passed] += data[i,2]
        cases_array[days_passed] += data[i,3]

    start_day = int((min_date_country-min_date).total_seconds()/(3600.0*24))

    m,c = linear_reg(np.linspace(start_day,total_days,total_days-start_day+1)\
    [-start_regression_day-regression_days:-start_regression_day],\
    cases_array[-start_regression_day-regression_days:-start_regression_day])

    print("Based on data from the last",regression_days,"days,",country_name,"is doubling every",1.0/m,"days")
    return m, total_days

#def regression_plot(fname,country_name,start_day):
fname = sys.argv[1]
country_name = sys.argv[2]
start_day = int(sys.argv[3])
num_days_regression = int(sys.argv[4])
regression_days = np.linspace(start_day,1,start_day).astype(int)
Ndays = len(regression_days) 
doubling_time = np.zeros(Ndays)
total_days = 0
for i in range(Ndays):
    m, total_days = covid19_model(fname,country_name,num_days_regression,regression_days[i])
    if(abs(1.0/m) > 10000):    
        doubling_time[i] = 0
        continue
    doubling_time[i] = 1.0/m

